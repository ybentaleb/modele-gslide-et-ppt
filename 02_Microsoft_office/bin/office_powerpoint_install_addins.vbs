' Converti les fichiers PowerPoint vers différents formats
' Author: Philippe PRADOS

Const msoFalse = 0   ' False.
Const msoTrue = -1   ' True.

Set args=WScript.Arguments

Dim filename
filename = args.item(0)

'WScript.Echo "filename=" & filename

Set powerPoint = CreateObject("PowerPoint.Application")
powerPoint.Visible = True
Set AddIns=powerPoint.AddIns

For Each ad In AddIns
    If InStr(ad.Name, name) Then
'        WScript.Echo "AddIns.remove=" & ad.Name
        ad.Loaded = msoFalse
        AddIns.Remove ad.Name
    End If
Next

Dim component
'WScript.Echo "AddIns.add=" & filename
Set component = AddIns.add(filename)
component.Loaded = msoTrue

