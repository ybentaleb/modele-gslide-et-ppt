' Converti les fichiers PowerPoint vers différents formats
' Author: Philippe PRADOS

Const msoFalse = 0   ' False.
Const msoTrue = -1   ' True.

Set powerPoint = CreateObject("PowerPoint.Application")
Set AddIns=powerPoint.AddIns

For Each ad In AddIns

    If InStr(ad.Name, name) Then
        WScript.Echo "Remove " & ad.Name
        ad.Loaded = msoFalse
        AddIns.Remove ad.Name
    End If
Next
