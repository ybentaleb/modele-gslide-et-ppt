@chcp 1252 2>nul >nul
@echo off
REM Author: Philippe PRADOS

SET MILLESIME=2020-04
SET OCTO_TEMPLATES=C:\ProgramData\OCTO\Template
SET SLIDES_PREFORMATE=Slides_préformatés
rem SET ADDINS=%APPDATA%\Microsoft\AddIns
SET ADDINS=%OCTO_TEMPLATES%\%MILLESIME%


c:\Windows\system32\cscript.exe /nologo bin\office_powerpoint_remove_addins.vbs "OCTO_Tools"
reg delete HKCU\Software\Microsoft\Office\16.0\Common\Spotlight\Providers\OCTO /F >nul 2>&1
reg delete HKCU\Software\Microsoft\Office\16.0\Common\Spotlight\Providers\OCAC /F >nul 2>&1

reg query HKEY_CURRENT_USER\Software\Wine >nul 2>nul
IF %ERRORLEVEL% EQU 0 goto WINE
rmdir /Q "%OCTO_TEMPLATES%\%MILLESIME" 2>nul

:WINE

echo OCTO tools uninstalled