#!/usr/bin/env bash
# Install on Linux with Wine or CrossOver in the default bottle
# Author: Philippe PRADOS
set -e
cd "$(dirname $0)"

# Check the presence of Wine
which wine >/dev/null || ( echo "Install Wine or Crossover before, and Office 365 on a bottle" 1>&2 ; exit 1 )

export WINEPREFIX="${WINEPREFIX:~/.wine}"

# Check if CrossOver
if [[ -e ~/.cxoffice ]] ; then
# Assume qu'une seule bouteille existe avec le mot 'Office'
export WINEPREFIX=$(ls -d ~/.cxoffice/*Office*)
export CX_ROOT="${CX_ROOT:-$(dirname "$WINEPREFIX")}"
export CX_BOTTLE="${CX_BOOTLE:-$(basename "$WINEPREFIX")}"
# see https://www.codeweavers.com/support/docs/crossover-linux/cxbottlepath for CX_BOTTLE_PATH & CX_MANAGED_BOTTLE_PATH
fi

MODE=${1:-links}

MILLESIME=2020-04
DRIVE_C=${WINEPREFIX}/drive_c
OCTO_TEMPLATES=${DRIVE_C}/ProgramData/OCTO/Template/${MILLESIME}
SLIDES_PREFORMATE=Slides_préformatés
THEME=${OCTO_TEMPLATES}/Themes

rm -f "${OCTO_TEMPLATES}"

if [[ $MODE == links ]]; then
  echo "Use links"
  mkdir -p $(dirname ${OCTO_TEMPLATES})
  #ln -s "${PWD}/${MILLESIME}_OCTO_Tools.ppam" "${ADDINS}"
  ln -s "${PWD}" "${OCTO_TEMPLATES}"
else
  echo "Use copies"
  mkdir -p $(dirname ${OCTO_TEMPLATES})
  rm -rf "${THEME}/Themes Octo"
  cp -rf "${PWD}" "${OCTO_TEMPLATES}"
fi

wine cmd /c "$(realpath install-windows.bat)"
