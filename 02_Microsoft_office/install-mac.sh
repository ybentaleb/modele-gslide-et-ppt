#!/usr/bin/env bash
# Author: Philippe PRADOS
cd "$(dirname $0)"

MILLESIME=2020-04

MODE=${1:-links}
#ADDINS="${HOME}/Library/Group Containers/UBF8T346G9.Office/User Content.localized/Add-Ins.localized/"
OCTO_TEMPLATE="${HOME}/Library/Group Containers/UBF8T346G9.Office/User Content.localized/OCTO_TEMPLATEs.localized/"
THEME="${HOME}/Library/Group Containers/UBF8T346G9.Office/User Content.localized/Themes.localized"

#rm -f  "${ADDINS}/${MILLESIME}_OCTO_Tools.ppam"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCTO.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCTO.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCAC.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCAC.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_OCTO_TEMPLATE_Doc_OCTO.dotx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_OCTO_TEMPLATE_Sheet_OCTO.xltx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_4-3_Slides_préformatés_OCTO.pptx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_16-9_Slides_préformatés_OCTO.pptx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_Notice_OCTO_TEMPLATE_Powerpoint.pptx"
rm -rf "${THEME}/Themes Octo"

if [[ $MODE == links ]]; then
  echo "Use links"
  #ln -s "${PWD}/${MILLESIME}_OCTO_Tools.ppam" "${ADDINS}"
  ln -s "${PWD}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCTO.potx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCTO.potx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCAC.potx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCAC.potx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_OCTO_TEMPLATE_Doc_OCTO.dotx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_OCTO_TEMPLATE_Sheet_OCTO.xltx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_4-3_Slides_préformatés_OCTO.pptx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_16-9_Slides_préformatés_OCTO.pptx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/${MILLESIME}_Notice_OCTO_TEMPLATE_Powerpoint.pptx" "${OCTO_TEMPLATE}"
  ln -s "${PWD}/Themes" "${THEME}/Themes Octo"
else
  echo "Use copies"
  rm -rf "${THEME}/Themes Octo"
  #cp "${PWD}/${MILLESIME}_OCTO_Tools.ppam" "${ADDINS}"
  cp "${PWD}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCTO.potx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCTO.potx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCAC.potx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCAC.potx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_OCTO_TEMPLATE_Doc_OCTO.dotx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_OCTO_TEMPLATE_Sheet_OCTO.xltx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_4-3_Slides_préformatés_OCTO.pptx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_16-9_Slides_préformatés_OCTO.pptx" "${OCTO_TEMPLATE}"
  cp "${PWD}/${MILLESIME}_Notice_OCTO_TEMPLATE_Powerpoint.pptx" "${OCTO_TEMPLATE}"
  cp -r "${PWD}/Themes" "${THEME}/Themes"
fi

[ -e ~/Documents/Modèles\ Office\ personnalisés ] || ln -s "${OCTO_TEMPLATE}" ~/Documents/Modèles\ Office\ personnalisés

echo "OCTO Tools installed"