#!/usr/bin/env bash
# Author: Philippe PRADOS
cd "$(dirname $0)"

MILLESIME=2020-04

ADDINS="${HOME}/Library/Group Containers/UBF8T346G9.Office/User Content.localized/Add-Ins.localized/"
OCTO_TEMPLATE="${HOME}/Library/Group Containers/UBF8T346G9.Office/User Content.localized/OCTO_TEMPLATEs.localized/"
THEME="${HOME}/Library/Group Containers/UBF8T346G9.Office/User Content.localized/Themes.localized"

rm -f  "${ADDINS}/${MILLESIME}_OCTO_Tools.ppam"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCTO.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCTO.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_4-3_OCTO_TEMPLATE_Slide_OCAC.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_16-9_OCTO_TEMPLATE_Slide_OCAC.potx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_OCTO_TEMPLATE_Doc_OCTO.dotx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_OCTO_TEMPLATE_Sheet_OCTO.xltx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_4-3_Slides_préformatés_OCTO.pptx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_16-9_Slides_préformatés_OCTO.pptx"
rm -f  "${OCTO_TEMPLATE}/${MILLESIME}_Notice_OCTO_TEMPLATE_Powerpoint.pptx"
rm -rf "${THEME}/Themes Octo"

echo "OCTO tools uninstalled"