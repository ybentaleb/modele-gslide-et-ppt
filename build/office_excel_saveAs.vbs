' Converti les fichiers PowerPoint vers différents formats
' Author: Philippe PRADOS

' https://docs.microsoft.com/fr-fr/office/vba/api/excel.xlfileformat
Dim mapMode
Set mapMode = CreateObject("Scripting.Dictionary")
mapMode.CompareMode = vbTextCompare
mapMode.Add "xlOpenXMLTemplate", 54
mapMode.Add "xlOpenXMLTemplateMacroEnabled", 53
mapMode.Add "xlOpenXMLWorkbook", 51
mapMode.Add "xlOpenXMLWorkbookMacroEnabled", 52
mapMode.Add "xlTemplate",17
mapMode.Add "xlTemplate8",17

Set args=WScript.Arguments

Dim filename_source, filename_dest, mode
filename_source = args.item(0)
filename_dest = args.item(1)
mode = mapMode(args.item(2))

'WScript.Echo "source=" & filename_source
'WScript.Echo "dest=" & filename_dest

Set excel = CreateObject("Excel.Application")
Set exceldoc = excel.Workbooks.Open(filename_source)
if err.Number <> 0 then : WScript.Quit(-1) : end if
exceldoc.SaveAs filename_dest, mode
if err.Number <> 0 then : WScript.Quit(-1) : end if
exceldoc.close()

