#!/usr/bin/env bash
# Author: Philippe PRADOS
# Patch ppam pour ajouter la barre de bouton OCTO
# Injectin du répertoire patchs_ppam/ dans le fichier
filename=$(realpath $1)
dirname=$(dirname "$1")
extension="${filename##*.}"
basename="${filename%.*}"
path="$basename.$extension"
TEMP_DIR=${TEMP:=/tmp}/office

rm -rf "$TEMP_DIR"
rm -f "${basename}_fix.${extension}"
mkdir "$TEMP_DIR"
# Workaround pour gérer le pb de flush des disques réseaux sous WSL
if [[ "$(uname -r)" == *Microsoft ]] ; then
  sleep 10
fi
echo unzip $1 -d $TEMP_DIR
unzip $1 -d $TEMP_DIR
cp -Rf patchs_ppam/* "$TEMP_DIR/"
pushd "$TEMP_DIR"
zip -qr "${basename}_fix.${extension}" .
popd
rm -rf "$TEMP_DIR"
rm -f "$filename"
mv "${basename}_fix.${extension}" "$filename"
