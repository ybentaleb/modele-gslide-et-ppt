' Converti les fichiers PowerPoint vers différents formats
' Author: Philippe PRADOS

Set args=WScript.Arguments

Dim filename_source, filename_dest, mode
filename_source = args.item(0)
msg = args.item(1)

'WScript.Echo "source=" & filename_source

Set powerPoint = CreateObject("PowerPoint.Application")
Set presentation = powerPoint.Presentations.Open(filename_source)
powerPoint.activate
MsgBox msg
