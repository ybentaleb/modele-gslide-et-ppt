' Converti les fichiers PowerPoint vers différents formats
' Author: Philippe PRADOS

' https://docs.microsoft.com/fr-fr/office/vba/api/PowerPoint.PpSaveAsFileType
Dim mapMode
Set mapMode = CreateObject("Scripting.Dictionary")
mapMode.CompareMode = vbTextCompare
mapMode.Add "ppSaveAsPresentation", 1
mapMode.Add "ppSaveAsTemplate", 5
mapMode.Add "ppSaveAsRTF", 6
mapMode.Add "ppSaveAsShow", 7
mapMode.Add "ppSaveAsAddIn", 8
mapMode.Add "ppSaveAsDefault", 11
mapMode.Add "ppSaveAsMetaFile", 15
mapMode.Add "ppSaveAsGIF", 16
mapMode.Add "ppSaveAsJPG", 17
mapMode.Add "ppSaveAsPNG", 18
mapMode.Add "ppSaveAsBMP", 19
mapMode.Add "ppSaveAsTIF", 21
mapMode.Add "ppSaveAsEMF", 23
mapMode.Add "ppSaveAsOpenXMLPresentation", 24
mapMode.Add "ppSaveAsOpenXMLPresentationMacroEnabled", 25
mapMode.Add "ppSaveAsOpenXMLTemplate", 26
mapMode.Add "ppSaveAsOpenXMLTemplateMacroEnabled", 27
mapMode.Add "ppSaveAsOpenXMLShow", 28
mapMode.Add "ppSaveAsOpenXMLShowMacroEnabled", 29
mapMode.Add "ppSaveAsOpenXMLAddin", 30
mapMode.Add "ppSaveAsOpenXMLTheme", 31
mapMode.Add "ppSaveAsPDF", 32
mapMode.Add "ppSaveAsXPS", 33
mapMode.Add "ppSaveAsXMLPresentation", 34
mapMode.Add "ppSaveAsOpenDocumentPresentation", 35
mapMode.Add "ppSaveAsOpenXMLPicturePresentation", 36
mapMode.Add "ppSaveAsWMV", 37
mapMode.Add "ppSaveAsStrictOpenXMLPresentation", 38
mapMode.Add "ppSaveAsMP4", 39
mapMode.Add "ppSaveAsAnimatedGIF", 40
mapMode.Add "ppSaveAsExternalConverter", 64000

On error resume next

Set args=WScript.Arguments

Dim filename_source, filename_dest, mode
filename_source = args.item(0)
filename_dest = args.item(1)
mode = mapMode(args.item(2))

'WScript.Echo "saveAs source=[" + filename_source + "]"
'WScript.Echo "saveAs dest=[" + filename_dest + "]"

Set powerPoint = CreateObject("PowerPoint.Application")
Set presentation = powerPoint.Presentations.Open(filename_source)
if err.Number <> 0 then : WScript.Echo "Impossible to open " & filename_source : WScript.Quit(-1) : end if
WScript.Sleep(1000)
presentation.SaveAs filename_dest, mode
if err.Number <> 0 then : WScript.Echo "Impossible to SaveAs to " & filename_dest : WScript.Quit(-1) : end if
WScript.Echo "Saved"
'presentation.close()
powerPoint.quit
