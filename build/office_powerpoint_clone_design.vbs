' Converti les fichiers PowerPoint vers différents formats
' Author: Philippe PRADOS

Set args=WScript.Arguments

Dim filename_source
Dim design
filename_source = args.item(0)

'WScript.Echo "source=" & filename_source

Set powerPoint = CreateObject("PowerPoint.Application")
Set presentation = powerPoint.Presentations.Open(filename_source)
Set design = presentation.Designs.Clone(presentation.Designs(1), 1)
presentation.Designs(2).Delete
presentation.save
presentation.close
