#!/usr/bin/env bash
# Author: Philippe PRADOS

set -e

# Patch l'export GDrive pour
# - Supprimer les polices de caractères invalides
# - Injecter un style pour les figures
function patch_gdrive_ppt() {
    echo "--- PATCH $1"
    if [[ ! -f "$1" ]] ; then
      echo "Error: file '$1' not found"
      return
    fi
    dirname=$(dirname "$1")
    filename=${dirname}/$(basename -- "$1")
    extension="${filename##*.}"
    basename="${filename%.*}"
    filename=$(realpath $filename)
    echo "Patch \"$filename\""

    TEMP_DIR=${TEMP:=/tmp}/office
    rm -rf $TEMP_DIR
    mkdir $TEMP_DIR
    # Extraction du style des dessins
    unzip -q "OCTO_Tools.pptm" -d $TEMP_DIR/Figures
    pushd $TEMP_DIR/Figures
    xmlstarlet sel -t -c "/p:presentation/p:defaultTextStyle" ppt/presentation.xml >../default-text-style.xml

    THEME=theme1
    xmlstarlet sel -t -c "/a:theme/a:objectDefaults" ppt/theme/${THEME}.xml >../object-defaults.xml
    xmlstarlet sel -t -c "/a:theme/a:themeElements/a:fontScheme" ppt/theme/${THEME}.xml >../style-fonts.xml
    popd

    # Injection dans le modèle exporté depuis GDrive, du style des dessins
    unzip -q "$filename" -d  $TEMP_DIR/source
    pushd $TEMP_DIR/source >/dev/null
    # Remplace les polices de caractères injectés par Google
    export LC_ALL=C
    OS=$(uname -r)
    if [[ "$OS" == "Darwin*" ]] ; then
      SED_I="sed -i ''"
    else
      SED_I="sed -i"
    fi
    # Supprime une police parasite de GSlide ?
    find . -type f -print0 | xargs -0 $SED_I 's/Noto Sans Symbols/Century Gothic/g'

    # Inject un style par défault pour les figures
    echo "Inject figures and font style..."
    THEME=theme1
    xmlstarlet ed -P -d "/a:theme/a:objectDefaults" ppt/theme/${THEME}.xml |
    xmlstarlet ed -P -i "a:theme/a:extraClrSchemeLst" -t elem -n octo  |
    { sed -e "s/<octo\/>/$(sed 's@[/\&]@\\&@g;$!s/$/\\/' ../object-defaults.xml)/" ; } |
    xmlstarlet ed -P -a "/a:theme/a:themeElements/a:fontScheme" -t elem -n octo |
    xmlstarlet ed -P -d "/a:theme/a:themeElements/a:fontScheme" |
    { sed -e "s/<octo\/>/$(sed 's@[/\&]@\\&@g;$!s/$/\\/' ../style-fonts.xml)/" ; } >ppt/theme/${THEME}.new.xml
    rm ppt/theme/${THEME}.xml
    mv ppt/theme/${THEME}.new.xml ppt/theme/${THEME}.xml

    # Injecter le style des bullets dans presentations.xml
    echo "Inject bullet style..."
    xmlstarlet ed -P -d "/p:presentation/p:defaultTextStyle" ppt/presentation.xml |
    xmlstarlet ed -P -i "/p:presentation/p:extLst" -t elem -n octo |
    { sed -e "s/<octo\/>/$(sed 's@[/\&]@\\&@g;$!s/$/\\/' ../default-text-style.xml)/" ; } >ppt/presentation.new.xml
    rm ppt/presentation.xml
    mv ppt/presentation.new.xml ppt/presentation.xml

    zip -qr "$filename.fix" .
    popd >/dev/null
    rm -rf $TEMP_DIR
    rm "$filename"
    mv "$filename".fix "$filename"
    echo "File '$1' patched"
}

#IFS=$'\n'
patch_gdrive_ppt "$1"
