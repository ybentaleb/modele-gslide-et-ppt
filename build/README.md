# Modèles OCTO compatibles Google Slide et Powerpoint - Version 2020

Ce projet permet de faire évoluer les modèles *Google Slide* et *Powerpoint* d'OCTO 
en parallèle, pour maintenir une compatibilité entre les deux.

## Les fichiers du projet
| Fichier | Usage |
|---|---|
| *.gdslides ou*.gdsheet ou *.gddoc | Lorque vous utilisez la synchronisation GDrive, des liens vers les documents GDrive. |
| build/Makefile | Un makefile pour aider à la release du projet (fonctionne sous bash) |
| build/Notice_Template_OCTO_Powerpoint_et_GSlide.gdslides | La source de la documentation, pour générer des versions différents pour Powerpoint et GSlide |
| build/OCTO_4-3_Slide.pptx | Un export du fichier 01_Google_drive/*_4-3_Template_Slide_OCTO.gdslides au format pptx |
| build/OCTO_16-9_Slide.pptx | Un export du fichier 01_Google_drive/*_16-9_Template_Slide_OCTO.gdslides au format pptx |
| build/OCAC_4-3_Slide.pptx | Un export du fichier 01_Google_drive/*_4-3_Template_Slide_OCAC.gdslides au format pptx |
| build/OCAC_16-9_Slide.pptx | Un export du fichier 01_Google_drive/*_16-9_Template_Slide_OCAC.gdslides au format pptx |
| build/OCTO_Doc.pptx | Un export du fichier 01_Google_drive/*_OCTO_Template_Doc.gdslides au format docx |
| build/OCTO_Sheet.xlsx | Un export du fichier 01_Google_drive/*_OCTO_Template_Sheet.gdslides au format xlsx |
| build/*_OCTO_Tools.pptm | Source des macros et des modèles de figures, à exporter sous forme de complément |
| build/office_excel_saveAs.vbs | Script de convertion des fichiers Excel |
| build/office_powerpoint_clone_design.vbs | Script de clone du template PPT à cause d'un bug GSlide |
| build/office_powerpoint_open_msg.vbs | Ouverture d'un fichier PPT avec message |
| build/office_powerpoint_saveAs.vbs | Conversion de fichiers PPT |
| build/office_word_saveAs.vbs | Conversion de fichiers Doc |
| build/patch_gdrive_export.sh | Script de nettoyage des exports PPTX de GDrive |
| build/patch_ppam.sh | Script d'injection de la description du ruban |
| build/README.md | Ce fichier pour la prise en main du projet |
| build/Schema_dependences_fichiers.svg | Schema de conversions des fichiers |
| 01_Google_drive/*_4-3_Slides_préformatés_OCTO.gdslides | Les slide pré-formaté 4/3 GSlide |
| 01_Google_drive/*_4-3_Template_Slide_OCAC.gdslides | Le modèle GSlide 4/3 OCAC |
| 01_Google_drive/*_4-3_Template_Slide_OCTO.gdslides | Le modèle GSlide 4/3 OCTO |
| 01_Google_drive/*_16-9_Slides_préformatés_OCTO.gdslides | Les slide pré-formaté 16/9 GSlide |
| 01_Google_drive/*_16-9_Template_Slide_OCAC.gdslides | Le modèle GSlide 16/9 OCAC |
| 01_Google_drive/*_16-9_Template_Slide_OCTO.gdslides | Le modèle GSlide 16/9 OCTO |
| 01_Google_drive/*_Notice_Template_GSlide.gdslides | La notice pour GDrive |
| 01_Google_drive/*_Template_Doc_OCTO.gdsheet | Un modèle Google Doc |
| 01_Google_drive/*_Template_Sheet_OCTO.gdsheet | Un modèle Google Sheet |
| 01_Google_drive/README.gddoc | La procédure d'installation pour Google Drive |
| 02_Microsoft_office/Themes | Les différents thèmes de couleurs OCTO à importer |
| 02_Microsoft_office/bin/office_powerpoint_install_addins.vbs | Script d'installation d'un add-in |
| 02_Microsoft_office/bin/office_powerpoint_remove_addins.vbs | Script de suppression d'un add-in |
| 02_Microsoft_office/*_4-3_Slides_préformatés_OCTO.pptx | Les slides pré-formaté 4/3 Powerpoint |
| 02_Microsoft_office/*_4-3_Template_Slide_OCAC.potx | Le modèle Powerpoint 4/3 OCAC |
| 02_Microsoft_office/*_4-3_Template_Slide_OCTO.potx | Le modèle Powerpoint 4/3 OCTO |
| 02_Microsoft_office/*_16-9_Slides_préformatés_OCTO.pptx | Les slides pré-formaté 16/9 Powerpoint |
| 02_Microsoft_office/*_16-9_Template_Slide_OCAC.potx | Le modèle Powerpoint 16/9 OCAC |
| 02_Microsoft_office/*_16-9_Template_Slide_OCTO.potx | Le modèle Powerpoint 16/9 OCTO |
| 02_Microsoft_office/*_Notice_Template_Powerpoint.pptx | La notice pour Powerpoint |
| 02_Microsoft_office/*_OCTO_Tools.ppam | Le complément à ajouter à Powerpoint pour avoir un menu |
| 02_Microsoft_office/*_Template_Doc_OCTO.dotx | Modèle pour Word |
| 02_Microsoft_office/*_Template_Sheet_OCTO.dotx | Modèle pour Excel |
| 02_Microsoft_office/install_mac.bat | Le script d'installation sous MacOS |
| 02_Microsoft_office/install_windows.bat | Le script d'installation sous Windows |
| 02_Microsoft_office/README.gddoc | La procédure d'installation pour Powerpoint |
| 02_Microsoft_office/uninstall_mac.bat | Le script de désinstallation sous MacOS |
| 02_Microsoft_office/uninstall_windows.bat | Le script de désinstallation sous Windows |

## Contribuer

Le code considère le modèle **Google Slide** comme la *référence*. 
C'est à partir de Google Slide que les fichiers Powerpoint sont produits. 
Par simplification, quelques manoeuvres doivent *encore* être faites à la main. 

Le code est développé sous un environnement Linux ET sous Windows 10, nécessitant d'installer 
le [sous-système ubuntu](https://docs.microsoft.com/fr-fr/windows/wsl/install-win10).

Vous devez synchroniser les fichiers GDrive avec [InSync](https://www.insynchq.com/) ou
[Google Backup and Sync](https://www.google.com/intl/fr_ALL/drive/download/backup-and-sync/). 
Des vérifications de cohérences supplémentaires sont alors possibles.

Il faut également ouvrir les droits vers l'API de GDrive. Pour cela, obtenez le fichier `credentials.json`.
Tapez `make configure`

Le projet mélange la sauvegarde des fichiers sources via GIT et via Google Drive. 
Seul les fichiers de développement sont sous GIT. Les autres Powerpoints ou 
Googles Slides, sont sous la responsabilité de GDrive.

## Release

![Dependences|width=400](Schema_dependences_fichiers.svg)

Pour publier une release, il faut partir des dernières versions des fichiers GDrive, 
et export/manipuler vers différents formats, à différentes localisations.

Pour simplifier la procédure et ne rien oublier, un `Makefile` est là pour nous aider.

La première chose à faire est de lancer un `bash`. Assurez-vous d'utiliser le bash du sous-système Ubuntu de Windows.
```bash
C:\>bash
$ uname -v
#476-Microsoft Fri Nov 01 16:53:00 PST 2019
```
Assurez-vous d'avoir installé les packages suivant:
```
$ sudo apt-get install make xmlstarlet jq
```

Puis, lancez la release
```bash
$ cd build
$ make full_release
```

## Export en Powerpoint
Le code demande à GDrive d'exporter les fichiers en Powerpoint. Cela n'est pas suffisant. En effet,
l'export de GDrive n'intègre pas les styles des graphiques ou la police par défaut, utilisée pour
la création de tableau.
De plus, des bugs dans les fichiers exportés par GSlide nécessites des interventions fines dans les fichiers.
Nous utilisons alors un script qui se charge de récupérer ces informations depuis le fichier `OCTO_Tools.pptm`
pour les injecter dans le fichier exporté.

### Le complément
Pour générer le *complément*, il faut 
1) modifiez le fichier `*_OCTO_Tools.pptm`, 
2) l'`Enregistrer`, 
3) l'`Enregistrer sous` / `Complément Powerpoint (*.ppam)` / `Autre options` / sélectionnez le répertoire 
`../02_Microsoft_office` et validez.
4) appliquer la commande `./patch_ppam.sh ../02_Microsoft_office/*_OCTO_Tools.ppam` pour injecter le rubban et les icônes
5) copier le fichier 

Cela est fait en partie automatiquement par le `Makefile`.
```bash
$ make ../02_Microsoft_office/*_OCTO_Tools.ppam
```

Le `patch_ppam.sh` importe les fichiers du répertoire `patch_ppam` dans le 
module, afin de lui ajouter le ruban. Pour le modifier, il faut intervenir
sur les fichiers de ce répertoire et injecter à nouveau le résulat.

Le fichier `*_OCTO_Tools.pptm` possède des informations qui seront injectés dans d'autres fichiers.

Pour activer le débug du complément, suivre [ceci](https://www.thespreadsheetguru.com/blog/2014/11/6/view-powerpoint-add-in-code)

### Modifier le style de graphiques ou la police par défaut
Il faut paramétrer cela dans le fichier `*_OCTO_Tools.pptm`.
Les paramètres sont extrait de ce fichier pour être injectés dans les différentes présentations exportées.

Les exports en PowerPoint de Google Slide ne sont pas toujours propres.

### Les modèles/templates
Après avoir ajusté les modèles dans Google Slide, il faut les exporter et faire quelques 
manipulations. Des scripts du `Makefile` s'en chargent.

### Slide pré-formatés
Il est possible de publier un version Powerpoint des slides pré-formatés (uniquement sous Windows
à cause d'un bug de GSlide lors de l'export)
Pour cela, il faut :
- s'assurer d'avoir make 4+, `xmkstarlet` et `jq` d'installé
- lancer un `bash`
- faire un `make pre-formate` 
- et suivre la procédure.

### La documentation
La documentation est compatible Google Slide et Powerpoint. Des slides sont à supprimer suivant les cas.
Il faut également recalculer le sommaire.

## Thèmes
Pour ré-exporter les fichiers `../02_Microsoft_office/Themes/*.thmx` PowerPoint :
- Appliquer le thème de Google Slide via le menu du complément
- Capturer la palette avec Gimp
- Appliquer les couleurs dans Excel (Mise en Page / Couleur)
- Les sauvegarder
- Puis Thème / Enregistrer le thème actif

## Notes
- La commande `make help` expose d'autres commandes
- Il ne faut pas éditer les scripts VBA depuis MAC. Cela casse tous les sources.
- En cas de bug dans OCTO Tools, vérifier qu'il est bien possible de compiler tous le code.
- Pour générer le modèle 16/9 à partir du 4/5, faire une copie est :
    - Renommer le root des modèles
    - Modifier les tailles des titres des chapitres
    - revoir la barre de logo octo noir et blanc, sur tous les slides des modèles

### Liens utiles : 
https://www.labnol.org/internet/direct-links-for-google-drive/28356/

- Format d'URL Google : 
https://docs.google.com/presentation/d/{{Document ID}}/copy?id={{Document ID}}&copyCollaborators=false&copyComments=false&title=New+Research+Report&copyDestination={{Google Drive Folder ID}}

- Le plugin d'accenture
https://kxdocuments.accenture.com/contribution/204f6372-653e-48fa-a0a0-11ac5b1fb4e9

- Le premier modèle de template doit être le titre. Ainsi, un Ctrl-m produit le deuxieme.
Sinon, cela produit un slide du même type.

### Info Powerpoint
- Dans présentation.xml, on trouve les styles par défault pour les puces
- Dans notesMater1.xml, dans txtBody/lstStyle on trouve le style par défaut pour les notes.
- Dans theme2.xml, on trouve le style par défaut pour les couleurs et les fonts
- Pour gérer les puces et numéro, il faut dans un <a:p>, ajouter 
    - Rien pour avoir une puce
    - Pour supprimer la puce
        <a:pPr marL="0" indent="0">
            <a:buNone/>
        </a:pPr>
    - Pour utiliser un numéro
        <a:pPr marL="0" indent="-250000">
            <a:buFont typeface="+mj-lt"/>
            <a:buAutoNum type="arabicPeriod"/>
        </a:pPr>

### Installation des templates Office
Voir https://docs.microsoft.com/en-us/previous-versions/office/office-2010/cc178976(v=office.14)?redirectedfrom=MSDN
Voir https://social.technet.microsoft.com/Forums/en-US/fbc4bf23-c9fb-4a6c-a981-49f68127bf77/deploy-custom-templates-in-office-2010?forum=officesetupdeployprevious
Voir https://www.pdshop.com/locale-id-lcid-code-list-currency-date-kb-21/

Il faut alimenter une clé de la base de registre, pour faire référence à un fichier XML
qui possède la langue, et des références sur les templates.
La langue est portée par l'attribut `lcid`.
Pour faire des tests, il faut ouvrir la base de registre sur 
`HKEY_CURRENT_USER\Software\Microsoft\Office\16.0\Common\Spotlight\Providers`
et purger régulièrement la clé `Content`. Elle est alimenté la première fois qu'une application
découvre le template indiqué par `Providers`.

### Bug GSlide
- Actuellement, lorsque GSlide exporte un fichier au format Powerpoint, celui-ci est mal formé.
Conséquence, si on efface un slide associé à un template, le template correspondant est également effacé.
Pour le moment, nous utilisons un contournement consistant à cloner le modèle
et à effacer l'ancien, via un script vbs.

- Lorsque GSlide utilise un sous-titre et l'exporte, il y a un décalage sous Powerpoint, a cause
des offsets de première ligne ajouté par Powerpoint.

### Bug Powerpoint
- Pour résoudre le pb de template auto-destructeur, je fais un clone depuis PowerPoint
et je supprime le premier modèle. Dans ce cas,
les zones de textes de type "Body" sont calées à gauche.
Pour éviter cela, il faut :
- Soit refaire une zone de texte dans GSlide et y placer le contenu pour que la zone
ne soit pas de type body,
- Soit ajouter le style de la diapo en "Titre avec contenu"